# translation of kcmkwinrules.po to Uzbek
# Copyright (C) 2004, 2005 Free Software Foundation, Inc.
# Mashrab Kuvatov <kmashrab@uni-bremen.de>, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-07 02:19+0000\n"
"PO-Revision-Date: 2005-10-19 21:33+0200\n"
"Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>\n"
"Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>\n"
"Language: uz@cyrillic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr ""

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "%1 учун дастур мосламалари"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "%1 учун ойна мосламалари"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "Муҳим эмас"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr ""

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr ""

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "Доимий ифода"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr ""

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "Дарҳол қўллаш"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "Эслаб қолиш"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr ""

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr ""

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr ""

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr ""

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "%1 учун мосламалар"

#: rulesmodel.cpp:221
#, fuzzy, kde-format
#| msgid "Window settings for %1"
msgid "New window settings"
msgstr "%1 учун ойна мосламалари"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""

#: rulesmodel.cpp:382
#, fuzzy, kde-format
#| msgid "De&scription:"
msgid "Description"
msgstr "&Таъриф:"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Window"
msgid "Window matching"
msgstr "&Ойна"

#: rulesmodel.cpp:390
#, fuzzy, kde-format
#| msgid "Window &class (application type):"
msgid "Window class (application)"
msgstr "Ойнанинг &синфи (дастурнинг тури):"

#: rulesmodel.cpp:398
#, kde-format
msgid "Match whole window class"
msgstr ""

#: rulesmodel.cpp:405
#, fuzzy, kde-format
#| msgid "Window t&itle:"
msgid "Whole window class"
msgstr "Ойнанинг с&арлавҳаси:"

#: rulesmodel.cpp:411
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "Window types"
msgstr "Ойнанинг &турлари:"

#: rulesmodel.cpp:419
#, fuzzy, kde-format
#| msgid "Window t&itle:"
msgid "Window role"
msgstr "Ойнанинг с&арлавҳаси:"

#: rulesmodel.cpp:424
#, fuzzy, kde-format
#| msgid "Window t&itle:"
msgid "Window title"
msgstr "Ойнанинг с&арлавҳаси:"

#: rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Machine (hostname):"
msgid "Machine (hostname)"
msgstr "&Компьютернинг номи:"

#: rulesmodel.cpp:436
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Position"
msgstr "&Жойлашиши"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Size & Position"
msgstr "&Жойлашиши"

#: rulesmodel.cpp:442
#, fuzzy, kde-format
#| msgid "&Size"
msgid "Size"
msgstr "Ў&лчам"

#: rulesmodel.cpp:448
#, fuzzy, kde-format
#| msgid "Maximized &horizontally"
msgid "Maximized horizontally"
msgstr "&Энига ёйилган"

#: rulesmodel.cpp:453
#, fuzzy, kde-format
#| msgid "Maximized &vertically"
msgid "Maximized vertically"
msgstr "&Бўйига ёйилган"

#: rulesmodel.cpp:461
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktop"
msgstr "Ҳамма иш столлари"

#: rulesmodel.cpp:467
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktops"
msgstr "Ҳамма иш столлари"

#: rulesmodel.cpp:486
#, kde-format
msgid "Activities"
msgstr ""

#: rulesmodel.cpp:502
#, fuzzy, kde-format
#| msgid "Splash Screen"
msgid "Screen"
msgstr "Сплэш ойнаси"

#: rulesmodel.cpp:507
#, fuzzy, kde-format
#| msgid "&Fullscreen"
msgid "Fullscreen"
msgstr "&Бутун экранга"

#: rulesmodel.cpp:512
#, fuzzy, kde-format
#| msgid "M&inimized"
msgid "Minimized"
msgstr "Йиғи&лган"

#: rulesmodel.cpp:517
#, fuzzy, kde-format
#| msgid "Sh&aded"
msgid "Shaded"
msgstr "С&ояланган"

#: rulesmodel.cpp:522
#, fuzzy, kde-format
#| msgid "P&lacement"
msgid "Initial placement"
msgstr "Жойл&аштириш"

#: rulesmodel.cpp:531
#, kde-format
msgid "Ignore requested geometry"
msgstr ""

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""

#: rulesmodel.cpp:546
#, fuzzy, kde-format
#| msgid "M&inimum size"
msgid "Minimum Size"
msgstr "Энг &кичик ўлчам"

#: rulesmodel.cpp:551
#, fuzzy, kde-format
#| msgid "M&aximum size"
msgid "Maximum Size"
msgstr "Энг ка&тта ўлчам"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr ""

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr ""

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr ""

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr ""

#: rulesmodel.cpp:579
#, kde-format
msgid "Skip taskbar"
msgstr ""

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr ""

#: rulesmodel.cpp:585
#, kde-format
msgid "Skip pager"
msgstr ""

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""

#: rulesmodel.cpp:591
#, kde-format
msgid "Skip switcher"
msgstr ""

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "Тугмалар бирикмаси"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr ""

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr ""

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr ""

#: rulesmodel.cpp:614
#, kde-format
msgid "Active opacity"
msgstr ""

#: rulesmodel.cpp:619
#, kde-format
msgid "Inactive opacity"
msgstr ""

#: rulesmodel.cpp:625
#, kde-format
msgid "Focus stealing prevention"
msgstr ""

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr ""

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""

#: rulesmodel.cpp:680
#, kde-format
msgid "Accept focus"
msgstr ""

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""

#: rulesmodel.cpp:686
#, kde-format
msgid "Ignore global shortcuts"
msgstr ""

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""

#: rulesmodel.cpp:698
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Closeable"
msgstr "Ёпиб &бўладиган"

#: rulesmodel.cpp:703
#, fuzzy, kde-format
#| msgid "Window &type"
msgid "Set window type"
msgstr "Ойнанинг &тури"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr ""

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr ""

#: rulesmodel.cpp:766
#, fuzzy, kde-format
#| msgid "Window &class (application type):"
msgid "Window class not available"
msgstr "Ойнанинг &синфи (дастурнинг тури):"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""

#: rulesmodel.cpp:801
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "All Window Types"
msgstr "Ойнанинг &турлари:"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "Оддий ойна"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "Мулоқат ойнаси"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr ""

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr ""

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "Асбоблар панели"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr ""

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "Сплэш ойнаси"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "Иш столи"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr ""

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr ""

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "Ҳамма иш столлари"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr ""

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr ""

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr ""

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr ""

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr ""

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr ""

#: rulesmodel.cpp:869
#, fuzzy, kde-format
#| msgid "M&inimized"
msgid "Maximized"
msgstr "Йиғи&лган"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "Марказда"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "Тасодифий"

#: rulesmodel.cpp:872
#, fuzzy, kde-format
#| msgid "Top-Left Corner"
msgid "In Top-Left Corner"
msgstr "Юқори-чап бурчак"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "Сичқонча остида"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "Асосий ойна устида"

#: rulesmodel.cpp:881
#, fuzzy, kde-format
#| msgid "None"
msgid "None"
msgstr "Йўқ"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "Паст"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "Оддий"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "Юқори"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr ""

#: rulesmodel.cpp:928
#, fuzzy, kde-format
#| msgid "On Main Window"
msgid "Unmanaged window"
msgstr "Асосий ойна устида"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""

#: ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr ""

#: ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr ""

#: ui/main.qml:62
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr ""

#: ui/main.qml:63
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New…</interface> button below to add some"
msgstr ""

#: ui/main.qml:71
#, kde-format
msgid "Select the rules to export"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Unselect All"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Select All"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid "Save Rules"
msgstr ""

#: ui/main.qml:100
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add New…"
msgstr "Я&нги"

#: ui/main.qml:111
#, kde-format
msgid "Import…"
msgstr ""

#: ui/main.qml:119
#, kde-format
msgid "Cancel Export"
msgstr ""

#: ui/main.qml:119
#, fuzzy, kde-format
#| msgid "Edit..."
msgid "Export…"
msgstr "Таҳрирлаш"

#: ui/main.qml:209
#, fuzzy, kde-format
msgid "Edit"
msgstr "Таҳрирлаш"

#: ui/main.qml:218
#, kde-format
msgid "Duplicate"
msgstr ""

#: ui/main.qml:227
#, fuzzy, kde-format
msgid "Delete"
msgstr "&Аниқлаш"

#: ui/main.qml:240
#, kde-format
msgid "Import Rules"
msgstr ""

#: ui/main.qml:252
#, kde-format
msgid "Export Rules"
msgstr ""

#: ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr ""

#: ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr ""

#: ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] ""

#: ui/RulesEditor.qml:63
#, fuzzy, kde-format
#| msgid "Detect Window Properties"
msgid "No window properties changed"
msgstr "Ойнанинг хоссаларини аниқлаш"

#: ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""

#: ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Close"
msgstr "Ёпиб &бўладиган"

#: ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add Property..."
msgstr "Я&нги"

#: ui/RulesEditor.qml:98
#, fuzzy, kde-format
#| msgid "Detect Window Properties"
msgid "Detect Window Properties"
msgstr "Ойнанинг хоссаларини аниқлаш"

#: ui/RulesEditor.qml:114 ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr ""

#: ui/RulesEditor.qml:115 ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] ""

#: ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr ""

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr ""

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:60
#, fuzzy, kde-format
#| msgid "None"
msgid "No"
msgstr "Йўқ"

#: ui/RulesEditor.qml:278 ui/ValueEditor.qml:168 ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr ""

#: ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr ""

#: ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr ""

#: ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr ""

#, fuzzy
#~| msgid "Application settings for %1"
#~ msgctxt "Window caption for the application wide rules dialog"
#~ msgid "Edit Application-Specific Settings"
#~ msgstr "%1 учун дастур мосламалари"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Машраб Қуватов"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kmashrab@uni-bremen.de"

#, fuzzy
#~| msgid "Window t&itle:"
#~ msgid "Window Rules"
#~ msgstr "Ойнанинг с&арлавҳаси:"

#~ msgid "KWin"
#~ msgstr "KWin"

#, fuzzy
#~| msgid "Detect Window Properties"
#~ msgid "Select properties"
#~ msgstr "Ойнанинг хоссаларини аниқлаш"

#~ msgid "Override Type"
#~ msgstr "Алмаштириш тури"

#~ msgid "Unknown - will be treated as Normal Window"
#~ msgstr "Номаълум - оддий ойна сифатида кўринади"

#~ msgid "Information About Selected Window"
#~ msgstr "Танланган ойна ҳақида маълумот"

#~ msgid "Class:"
#~ msgstr "Синф:"

#~ msgid "Type:"
#~ msgstr "Тур:"

#~ msgid "Title:"
#~ msgstr "Сарлавҳа:"

#~ msgid "Machine:"
#~ msgstr "Компьютер:"

#~ msgid "C&lear"
#~ msgstr "&Тозалаш"

#~ msgid "Window-Specific Settings Configuration Module"
#~ msgstr "Махсус ойналар учун бошқарув модули"

#~ msgid "(c) 2004 KWin and KControl Authors"
#~ msgstr "(C) 2004, KWin ва KControl муаллифлари"

#~ msgid "&Modify..."
#~ msgstr "Ўзгар&тириш"

#~ msgid "Move &Up"
#~ msgstr "&Юқорига"

#~ msgid "Move &Down"
#~ msgstr "&Пастга"

#~ msgid "Edit Shortcut"
#~ msgstr "Тугмалар бирикмасини таҳрирлаш"

#~ msgid "&Desktop"
#~ msgstr "&Иш столи"

#~ msgid "Smart"
#~ msgstr "Ихчам"

#~ msgid "kcmkwinrules"
#~ msgstr "kcmkwinrules"

#~ msgid "Transparent"
#~ msgstr "Шаффоф"

#~ msgid "&Moving/resizing"
#~ msgstr "&Кўчириш/ўлчамини ўзгартириш"

#, fuzzy
#~| msgid "Title:"
#~ msgid "Tiled"
#~ msgstr "Сарлавҳа:"

#~ msgid "&Geometry"
#~ msgstr "&Геометрия"

#~ msgid "&Preferences"
#~ msgstr "&Мослаш"

#~ msgid "&No border"
#~ msgstr "&Чегарасиз"

#~ msgid "&Detect"
#~ msgstr "&Аниқлаш"
